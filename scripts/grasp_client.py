#!/usr/bin/env python

import rospy
#from robot_service import *
from geometry_msgs.msg import Pose
from sensor_msgs.msg import JointState
from grasp_demo.srv import *


class graspClient:
    def __init__(self,vel_thresh):
        self.vel_thresh=vel_thresh

    def pre_shape(self,js):
        rospy.wait_for_service('pre_shape')
        
        try:
            move_to_pre_shape = rospy.ServiceProxy('pre_shape', PreShape)
            resp1 = move_to_pre_shape(js)
            return resp1.reached_pose
        except rospy.ServiceException, e:
            print "Service call failed: %s"%e

    def enable_grasp(self):
        vel_thresh=self.vel_thresh
        rospy.wait_for_service('grasp')
        
        try:
            grasp_object = rospy.ServiceProxy('grasp', GraspAllegro)
            resp1 = grasp_object(vel_thresh)
            return resp1.final_state
        except rospy.ServiceException, e:
            print "Service call failed: %s"%e
    
    
if __name__=='__main__':


    g_client=graspClient(0.1)
    
    # Publish pregrasp
    pre_grasp=np.zeros(16) 
    pre_grasp[12]=1.5
    pre_grasp[14]=0.1
    pre_grasp[15]=0.1
    js=JointState()
    js.position=pre_grasp

    print g_client.pre_shape(js)
    raw_input('enter to grasp')
    fin_state=g_client.enable_grasp()
    #print fin_state.position
    #print pre_grasp
    #raw_input("Enter to move to pre_grasp")
    #allegro_robot.publish_grasp(pre_grasp)
    #raw_input("Moved to object pose")

    # Send grasp command
    







