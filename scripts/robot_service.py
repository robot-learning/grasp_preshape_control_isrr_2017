# Listens to allegro joint states
import rospy
from rosgraph_msgs.msg import Clock
from sensor_msgs.msg import JointState
from geometry_msgs.msg import PoseStamped
from std_msgs.msg import Int8
import tf
from subprocess import Popen
import subprocess
import numpy as np
import copy

from kdl_allegro_model import *
from grasp_demo.srv import *

class robotStates:
    def __init__(self,node_name='allegro_node',publish_prefix='/allegro_hand_right',listen_prefix='/allegro_hand_right',rate=100.0,listen_aruco=False,clock_bag=False,listen_object=False,publish_palm=False):
        rospy.init_node(node_name)
        self.rate=rospy.Rate(rate)
        #self.switch_cmd=rospy.Publisher(publish_prefix+'/control_type',Int8,queue_size=10)

        self.robot_joint_cmd_pub=rospy.Publisher(publish_prefix+'/joint_cmd',
                                                     JointState, queue_size=100)

        rospy.Subscriber(listen_prefix+'/joint_states',JointState,self.joint_state_cb)
        self.clock=0
        if(listen_aruco):
            self.listener = tf.TransformListener()
        if(clock_bag):
            rospy.Subscriber('/clock',Clock,self.clock_callback)
            print 'Subscribing to clock'
        if(listen_object):
            rospy.Subscriber('/object/pose',self.obj_callback)
        if(publish_palm):
            self.palm_pose_pub=rospy.Publisher('lbr4_allegro/palm/goal_pose',PoseStamped,queue_size=1)
        
        #print 'Initialized robot node'
        self.loop_rate=rospy.Rate(rate)
        jc=JointState()
        jc.name = ['index_joint_0','index_joint_1','index_joint_2', 'index_joint_3',
                   'middle_joint_0','middle_joint_1','middle_joint_2', 'middle_joint_3',
                   'ring_joint_0','ring_joint_1','ring_joint_2', 'ring_joint_3',
                   'thumb_joint_0','thumb_joint_1','thumb_joint_2', 'thumb_joint_3']

        jc.position=[ 0.0, 0.0, 0.0, 0.0,
                      0.0, 0.0, 0.0, 0.0,
                      0.0, 0.0, 0.0, 0.0,
                      0.0, 0.0, 0.0, 0.0]
        self.jc=jc
        self.clock_bag=clock_bag
        self.kdl_model=allegroKinematics(60,10,[0,1,2,3])
        self.grasp_service=rospy.Service('grasp',GraspAllegro,self.grasp_velocity)
        rospy.loginfo('Service grasp:')
        rospy.loginfo('Ready to grasp the object.')
        #self.pre_shape_service=rospy.Service('pre_shape',PreShape,self.publish_grasp_service)


    def loop(self):
        while not rospy.is_shutdown():
            self.loop_rate.sleep()
        
    def palm_pub(self,pose):
        i=0
        while (not rospy.is_shutdown() and i<10):
            new_pose=PoseStamped()
            new_pose.header.frame_id='/world'
            new_pose.pose=pose
            self.palm_pose_pub.publish(new_pose)
            i+=1
            self.loop_rate.sleep()
            
    def obj_callback(self,msg):
        self.object_pose=msg.pose
    def reinitialize_tf(self):
        self.listener = tf.TransformListener()
      
    def clock_callback(self,msg):
        self.clock=msg.clock#copy.deepcopy(msg.clock)

        #print msg

    def tf_pose_pub(self,pose,target_frame,base_frame='palm_link'):
        tf_pose=tf.TransformBroadcaster()
        pose=pose.tolist()
        if(len(pose)==3):
            pose.extend([0.0,0.0,0.0])
        while not rospy.is_shutdown():
            for i in range(10):        
                tf_pose.sendTransform((pose[0],pose[1],pose[2]),tf.transformations.quaternion_from_euler(pose[3],pose[4],pose[5]),rospy.Time.now(),target_frame,base_frame)
                self.loop_rate.sleep()
            break
    
    def tf_pose_pub_process(self,pose,target_frame,base_frame='palm_link',rate=500):
        tf_pose=tf.TransformBroadcaster()
        pose=pose.tolist()
        if(len(pose)==3):
            pose.extend([0.0,0.0,0.0])
        quat=tf.transformations.quaternion_from_euler(pose[3],pose[4],pose[5])
        #x y z qx qy qz qw frame_id child_frame_id  period_in_ms
        args=[str(pose[0]),str(pose[1]),str(pose[2]),str(quat[0]),str(quat[1]),str(quat[2]),str(quat[3]),base_frame,target_frame,str(rate)]
        cmd=['/bin/bash','rosrun','tf','static_transform_publisher']+args
        proc=Popen(cmd,stdout=subprocess.PIPE)
        return proc

    def record_bag_process(self,file_name,topics=['/tf','/camera/rgb/image_raw','/camera/rgb/camera_info','/allegro_hand_right/joint_states','/allegro_hand_right/joint_cmd']):
        cmd=['/bin/bash','rosrun','rosbag','record','-O',file_name]+topics
        proc=Popen(cmd,stdout=subprocess.PIPE)
        return proc

    def run_process(self,cmd_args):
        cmd=cmd_args
        proc=Popen(cmd,stdout=subprocess.PIPE)
        return proc
    def tf_find_transform(self,target_frame,base_frame='palm_link'):
        got_tf=False
        if (self.clock_bag):
            time=copy.deepcopy(self.clock)
        else:
            time=copy.deepcopy(rospy.Time(0))

        time=copy.deepcopy(rospy.Time(0))
        (trans_2,rot_2)= self.listener.lookupTransform(base_frame, target_frame, rospy.Time(0))
        final_pose=np.zeros(6)
        final_pose[0:3]=trans_2
        final_pose[3:6]=tf.transformations.euler_from_quaternion(rot_2)
        got_tf=True
        #print 'tf'
    
        return final_pose

    def joint_state_cb(self,joint_state):
        self.joint_states=joint_state
        


    def publish_grasp(self,joint_pos_arr):
        self.jc.position=joint_pos_arr
        while not rospy.is_shutdown():
            for i in range(10):
                self.robot_joint_cmd_pub.publish(self.jc)
                self.loop_rate.sleep()
            break
        
    def publish_grasp_service(self,req):
        joint_pos_arr=req.pre_shape.position
        self.jc.position=joint_pos_arr
        pos_thresh=0.2
        while not rospy.is_shutdown():
            joint_diff=np.array(self.joint_states.position)-np.array(joint_pos_arr)
            if(all(abs(j_pos)<=pos_thresh for j_pos in joint_diff)==True):
                break
            self.robot_joint_cmd_pub.publish(self.jc)
            self.loop_rate.sleep()
            
        return True

    def publish_joint_trajectory(self,joint_pos_traj):
        while not rospy.is_shutdown():
            for i in range(len(joint_pos_traj)):
                self.jc.position=joint_pos_traj[i]
                self.robot_joint_cmd_pub.publish(self.jc)
                self.loop_rate.sleep()
            break
        print 'Finished running trajectory'

    def grav_comp_mode(self):
        sw_value=Int8()
        sw_value.data=2
        while not rospy.is_shutdown():
            for i in range(100):
                self.switch_cmd.publish(sw_value)
                self.rate.sleep()
            break

    def position_mode(self):
        sw_value=Int8()
        sw_value.data=0
        while not rospy.is_shutdown():
            for i in range(100):
                self.switch_cmd.publish(sw_value)
                self.rate.sleep()
            break

    def grasp_velocity(self, req):
        vel_thresh=req.joint_vel_thresh
        grasped_object=False
        j=0
        while (not grasped_object and not rospy.is_shutdown() and j<50):
            curr_pos=np.array(self.joint_states.position)
            curr_vel=self.joint_states.velocity
            
            new_cmd = np.copy(curr_pos)
            if req.top_grasp:
                #new_cmd[1] += 0.1 
                #new_cmd[5] += 0.1 
                #new_cmd[9] += 0.1 
                #new_cmd[14] += 0.1
                new_cmd[1:3] += 0.1 
                new_cmd[5:7] += 0.1 
                new_cmd[9:11] += 0.1 
                new_cmd[14:] += 0.1
            else:
                new_cmd[1:4] += 0.1 
                new_cmd[5:8] += 0.1 
                new_cmd[9:12] += 0.1 
                new_cmd[14:] += 0.15

            if(j>10):
                # Check for contact with object
                for i in range(len(curr_vel)):
                    if(abs(curr_vel[i])<=vel_thresh):
                        new_cmd[i]=curr_pos[i]
           
            self.publish_grasp(new_cmd)
            curr_pos=np.array(self.joint_states.position)
            curr_vel=self.joint_states.velocity

            if(j>10):
                # Check for contact with object
                if(all(abs(j_vel)<=vel_thresh for j_vel in curr_vel)==True):
                    grasped_object=True
                    break
                
            # Check if within joint limits
            j+=1

        rospy.loginfo('Service grasp:')
        if(grasped_object):
            rospy.loginfo('Grasped object.')
        else:
            rospy.loginfo('Failed to grasp object.')
            
        response = GraspAllegroResponse()
        response.final_joint_state =  self.joint_states
        response.success = True
        return response

    #def grasp_velocity(self,req):
    #    vel_thresh=req.joint_vel_thresh
    #    grasped_object_=False
    #    j=0
    #    #while (not grasped_object_ and not rospy.is_shutdown() and j<100):
    #    #    curr_pos=np.array(self.joint_states.position)
    #    #    curr_vel=self.joint_states.velocity
    #    #    
    #    #    new_cmd=curr_pos
    #    #    new_cmd[0:12]=curr_pos[0:12]+0.1
    #    #    new_cmd[12:15]=curr_pos[12:15]+0.4
    #    #    new_cmd[15]+=0.1

    #    #    new_cmd[0]=0.0
    #    #    new_cmd[4]=0.0
    #    #    new_cmd[8]=0.0
    #    #    new_cmd[12]=1.5
    #    #    new_cmd[13]=0.0
    #    while (not grasped_object_ and not rospy.is_shutdown() and j<50):
    #        curr_pos=np.array(self.joint_states.position)
    #        curr_vel=self.joint_states.velocity
    #        
    #        new_cmd=curr_pos
    #        new_cmd[0:12]=curr_pos[0:12]+0.05
    #        new_cmd[12:15]=curr_pos[12:15]+0.2
    #        new_cmd[15]+=0.1

    #        new_cmd[0]=curr_pos[0]#0.0
    #        new_cmd[4]=curr_pos[4]#0.0
    #        new_cmd[8]=curr_pos[8]#0.0
    #        new_cmd[12]=curr_pos[12]#1.5
    #        new_cmd[13]=curr_pos[13]#0.0

    #        if(j>10):
    #            # Check for contact with object
    #            for i in range(len(curr_vel)):
    #                if(abs(curr_vel[i])<=vel_thresh):
    #                    new_cmd[i]=curr_pos[i]
    #       
    #        self.publish_grasp(new_cmd)
    #        #self.loop_rate.sleep()
    #        curr_pos=np.array(self.joint_states.position)
    #        curr_vel=self.joint_states.velocity
    #        if(j>10):
    #            # Check for contact with object
    #            if(all(abs(j_vel)<=vel_thresh for j_vel in curr_vel)==True):
    #                grasped_object_=True
    #            
    #        # Check if within joint limits
    #        j+=1
    #        #print i

    #    rospy.loginfo('Service grasp:')
    #    if(grasped_object_):
    #        rospy.loginfo('Grasped object.')
    #    else:
    #        rospy.loginfo('Failed to grasp object.')
    #        
    #    response = GraspAllegroResponse()
    #    response.final_joint_state =  self.joint_states
    #    response.success = True
    #    return response
