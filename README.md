ROS package to perform simple grasping given a preshape.

1. Import into catkin workspace
2. Run scripts/grasp_service.py to start grasping service.
3. Grasp client class is in scripts/grasp_client.py.